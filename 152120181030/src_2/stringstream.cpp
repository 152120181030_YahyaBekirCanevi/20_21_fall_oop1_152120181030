#include <sstream>
#include <vector>
#include <iostream>
using namespace std;

vector<int> parseInts(string str) {
    vector<int> r;
    size_t a = 0;
    int x = 0;
    while ((a = str.find(',')) != string::npos) {
        stringstream s(str.substr(0, a));
        s >> x;
        r.push_back(x);
        str.erase(0, a + 1);
    }
    stringstream s(str);
    s >> x;
    r.push_back(x);

    return r;
}

int main() {
    string str;
    cin >> str;
    vector<int> integers = parseInts(str);
    for (int i = 0; i < integers.size(); i++) {
        cout << integers[i] << "\n";
    }

    return 0;
}