#include <iostream>
#include <string>
using namespace std;

int main() {
    string a, b;
    cin >> a >> b;

    cout << a.size() << " " << b.size() << endl;

    cout << a + b << endl;

    string a_ = a, b_ = b;

    a_[0] = b[0];
    b_[0] = a[0];

    cout << a_ << " " << b_ << endl;

    return 0;
}