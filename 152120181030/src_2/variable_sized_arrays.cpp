#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int n, q;

    cin >> n >> q;
    int** a = new int* [n];

    for (int i = 0; i < n; i++) {
        int s;
        cin >> s;
        a[i] = new int[s];
        for (int j = 0; j < s; j++) {
            cin >> a[i][j];
        }
    }

    for (int s = 0; s < q; s++) {
        int i, j;
        cin >> i >> j;
        cout << a[i][j] << endl;
    }

    return 0;
}