#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <string>
#include <algorithm>
using namespace std;
#define SIZE 20

void SeperateTags(string* ss, string* tag)
{
    string s_ = *ss, t, str, last;
    size_t a, b;

    if (*tag != "") {
        s_.erase(0, (*tag).length() + 1);
        string dels = "</" + *tag + ">";
        size_t d = s_.find(dels);
        s_.erase(d, s_.length());
        if (s_ == "") {
            *ss = "";
        }
        size_t c;
        if ((c = s_.find("<")) != string::npos) s_ = s_.substr(c, s_.length() - 1);
        a = s_.find(" ");
        t = s_.substr(1, a - 1);
        *ss = s_;
    }
    else {
        a = s_.find(" ");
        t = s_.substr(1, a - 1);
        s_.erase(0, a + 1);
    }

    b = s_.find("</");
    while (1) {
        if (s_ == "") {
            last = "";
            break;
        }
        str = s_.substr(b + 2, t.length());
        if (str == t) {
            last = (*ss).substr(0, a + 1 + b + t.length() + 3);
            s_.erase(0, b + t.length() + 3);
            break;
        }
        else {
            a += b + 4;
            s_.erase(0, b + 4);
            b = s_.find("</");
        }
    }
    *ss = last;
    *tag = t;
}

string SearchAttributes(string str, string tag, string q) {
    size_t c = str.find("<");
    size_t d = str.find(">");
    str = str.substr(c + 1, d);
    str = str.substr(tag.length() + 1, str.length());
    str.pop_back();
    vector <string> s;
    string sub;
    size_t a = 0;
    int x = 0;
    while ((a = str.find(" ")) != string::npos) {
        sub = str.substr(0, a);
        if (sub.find('=') == std::string::npos) s.push_back(sub);
        str.erase(0, a + 1);
    }
    s.push_back(str);
    for (int i = 0; i < s.size(); i++) {
        if (i % 2 == 1) s[i] = s[i].substr(1, s[i].length() - 2);
    }
    string ans = "";
    for (int i = 0; i < s.size() - 1; i++) {
        if (q == s[i]) ans = s[i + 1];
    }
    return ans;
}

int main() {
    int n, q;
    cin >> n >> q;
    cin.ignore();
    string s = "";
    for (int i = 0; i < n; i++) {
        string s1;
        getline(cin, s1);
        s += s1 + " ";
    }
    for (int f = 0; f < q; f++) {
        string s1, s2;
        getline(cin, s2);
        size_t a = 0, b = 0;
        a = s2.find("~");
        s1 = s2.substr(0, a);//tags
        s2.erase(0, a + 1);//attribute that is searching
        while ((b = s1.find(".")) != string::npos) {
            s1.erase(0, b + 1);
        }
        string str = s, str_ = "", tag = "";
        bool is = true;
        while (tag != s1) {
            if (s == "") {
                is = false;
                break;
            }
            if (str == "") {
                size_t z = s.find(str_);
                s.erase(0, z);
            }
            else {
                str_ = str;
            }
            SeperateTags(&str, &tag);
        }
        if (is == true) {
            string ans = SearchAttributes(str, tag, s2);
            if (ans == "") {
                cout << "Not Found!" << endl;
            }
            else {
                cout << ans << endl;
            }
        }
        else continue;
    }
    return 0;
}