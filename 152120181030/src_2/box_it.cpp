#include<bits/stdc++.h>

using namespace std;

class Box {
    int l, b, h;
public:
    Box() {
        l = 0;
        b = 0;
        h = 0;
    }
    Box(int l_, int b_, int h_) {
        l = l_;
        b = b_;
        h = h_;
    }
    Box(const Box& x) {
        l = x.l;
        b = x.b;
        h = x.h;
    }
    int getLength() { return l; }
    int getBreadth() { return b; }
    int getHeight() { return h; }
    long long CalculateVolume() { return (long long)l * b * h; }
    bool operator<(Box const& B) {
        if (l < B.l) { return true; }
        else if (b < B.b && l == B.l) { return true; }
        else if (h < B.h && b == B.b && l == B.l) { return true; }
        else { return false; }
    }
    friend ostream& operator<<(ostream& out, Box& B);
};

ostream& operator<<(ostream& out, Box& B) {
    out << B.getLength() << ' ' << B.getBreadth() << ' ' << B.getHeight();
    return out;
}

