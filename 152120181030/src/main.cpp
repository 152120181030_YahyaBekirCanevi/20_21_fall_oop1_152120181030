#include <iostream>
#include <fstream>

using namespace std;

/**
* Sum of elements of an array
*
* @param i -> index of the term in the array
* @param size -> size of the array
* @return Sum of array A
*/
int Sum(int A[], int i, int size);

/**
* Product of elements of an array
*
* @param i -> index of the term in the array
* @param size -> size of the array
* @return Product of array A
*/
int Product(int A[], int i, int size);

/**
* Find the smallest element by lineer search algorithm in an array
*
* @param size -> size of the array
* @return Smallest element in array A
*/
int Smallest(int A[], int size);

int main()
{
	ifstream f("input.txt"); /* Read from file */

	if (!f.is_open())
	{
		cout << "Error:'input.txt' There is no such file." << endl;
		system("pause");
		return 0;
	}

	int size; /* Size of the array*/

	///Read first line of the file to get size of the array
	f >> size; /* number of the elements */

	int* numbers = new int[size]; /* Dynamic Array */
	int i = 0; /*index of the term of the array*/

	while (!f.eof())
	{
		int s; /*temporary value*/
		f >> s; /// Read integer values from file
		numbers[i] = s; /// Performs the assignment of the elements of the array
		i++;
	}
	f.close();

	/// Print values if there are at least 1 elements in the file
	if (size > 0)
	{
		int sum = Sum(numbers, 0, size); /* Sum of the elements */
		float avg = (float)sum / size; /* Average of the elements */
		int prod = Product(numbers, 0, size); /* Product of the elements */
		int small = Smallest(numbers, size); /* Find the smallest element in the array */

		/// Print the sum, average, product and the smallest values on screen
		cout << "Sum is " << sum << endl;
		cout << "Product is " << prod << endl;
		cout << "Average is " << avg << endl;		
		cout << "Smallest is " << small << endl;
	}
	else
	{
		cout << "There is no elements." << endl;
	}

	/// Delete the dynamic array
	delete[] numbers;

	system("pause");
	return 0;
}

int Sum(int A[], int i, int size)
{
	if (i == size)///If i is equal to size, return 0 to end recursion
		return 0;
	return Sum(A, i + 1, size) + A[i];
}

int Product(int A[], int i, int size)
{
	if (i == size)///If i is equal to size, return 1 to end recursion
		return 1;
	return Product(A, i + 1, size) * A[i];
}

int Smallest(int A[], int size)
{
	int min = A[0];
	for (int i = 1; i < size; i++)///Lineer Search for finding the smallest element in array
	{
		if (min > A[i])
		{
			min = A[i];
		}
	}
	return min;
}